<?php

namespace QHO\Quote\Model\ResourceModel;

class Quote extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {
    protected function _construct() {
        $this->_init("quote", "entity_id");
    }
}