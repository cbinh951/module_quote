<?php

namespace QHO\Quote\Model\ResourceModel\Quote;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\
AbstractCollection {
    // protected $_idFieldName = "entity_id";

    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategy,
        \Magento\Framework\Event\ManagerInterface $eventManager,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\DB\Adapter\AdapterInterface $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null
    ) {
        $this->_init(
            'QHO\Quote\Model\Quote',
            'QHO\Quote\Model\ResourceModel\Quote'
        );
        parent::__construct(
            $entityFactory, $logger, $fetchStrategy, $eventManager, $connection,
            $resource
        );
        $this->storeManager = $storeManager;
    }

    public function getListQuote()
    {        
        $this->getSelect()->join(
            ['secondTable' => $this->getTable('quote_address')],
            'main_table.entity_id = secondTable.quote_id',
            ['created_at','updated_at','main_table.customer_email', 
            'email',
            'customer_name'=>'CONCAT(firstname," ",lastname) AS Fullname', 
            'firstname', 'lastname',  'street', 'city', 'region', 'postcode', 'telephone']
        )
        ->join(
            ['thirdTable' => $this->getTable('quote_item')],
            'main_table.entity_id = thirdTable.quote_id',
            ['sku']			
        )
        //  ->where("(secondTable.email IS NOT NULL OR secondTable.telephone IS NOT NULL) AND secondTable.address_type = 'shipping' AND reserved_order_id IS NULL AND items_count > 0")
       ->where("(secondTable.email IS NOT NULL OR secondTable.telephone IS NOT NULL) AND secondTable.address_type = 'shipping' AND  items_count > 0")
        ->group("main_table.entity_id");
    }
}