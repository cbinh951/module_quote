<?php

namespace QHO\Quote\Model;

class Quote extends \Magento\Framework\Model\AbstractModel {
    protected function _construct() {
        $this->_init("QHO\Quote\Model\ResourceModel\Quote");
    }
}