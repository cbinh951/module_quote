<?php 

namespace QHO\Quote\Block\Adminhtml\Quote\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Product extends Generic implements TabInterface {

    public function __construct(
                            Context $context, 
                            Registry $registry, 
                            FormFactory $formFactory,
                            array $data = []) {
        parent::__construct($context, $registry, $formFactory, $data);                            
    }

    protected function _prepareForm() {
        $form = $this->_formFactory->create();
        $fieldset = $form->addFieldset(
            "base_fieldset",
            ["legend"   =>  __("Product Reviews"), "class"  =>  "fieldset-wide"]
        );

        $fieldset->addField(
            "name",
            "label",
            [
                "name"  =>  "name",
                "label" =>  __("Name: "),
                "disabled"  =>  true
            ]
        );

        $fieldset->addField(
            "sku",
            "label",
            [
                "name"  =>  "sku",
                "label" =>  __("Sku:"),
                "disabled"  =>  true
            ]
        );

        $data = $this->_coreRegistry->registry("quote");

        $form->setValues($data[0]);

        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function getTabLabel() {
        return __("Main Information");
    }

    public function getTabTitle() {
        return __("Main Information");
    }

    public function canShowTab() {
        return true;
    }

    public function isHidden() {
        return false;
    }
}