<?php 

namespace QHO\Quote\Block\Adminhtml\Quote\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;
use Magento\Backend\Block\Widget\Tab\TabInterface;

class Customer extends Generic implements TabInterface {

    public function __construct(
                            Context $context, 
                            Registry $registry, 
                            FormFactory $formFactory,
                            array $data = []) {
        parent::__construct($context, $registry, $formFactory, $data);                            
    }

    protected function _prepareForm() {
        $form = $this->_formFactory->create();
        $data = $this->_coreRegistry->registry("quote");

        $fieldset = $form->addFieldset(
            "base_fieldset",
            ["legend"   =>  __("General Information"), "class"  =>  "fieldset-wide"]
        );

        if (!empty($data[0])) {
            $fieldset->addField(
                "entity_id",
                "hidden",
                ["name" =>  "id"]
            );
        }

        $fieldset->addField(
            "firstname",
            "label",
            [
                "name"  =>  "firstname",
                "label" =>  __("First Name:"),
                "disabled"  =>  true
            ]
        );

        $fieldset->addField(
            "lastname",
            "label",
            [
                "name"  =>  "lastname",
                "label" =>  __("Last Name:"),
                "disabled"  =>  true
            ]
        );

        $fieldset->addField(
            "email",
            "label",
            [
                "name"  =>  "email",
                "label" =>  __("Email:"),
                "disabled"  =>  true
            ]
        );

        $fieldset->addField(
            "telephone",
            "label",
            [
                "name"  =>  "telephone",
                "label" =>  __("Phone:"),
                "disabled"  =>  true
            ]
        );

        $fieldset->addField(
            "street",
            "label",
            [
                "name"  =>  "street",
                "label" =>  __("Street:"),
                "disabled"  =>  true
            ]
        );


        $form->setValues($data[0]);

        $this->setForm($form);
        return parent::_prepareForm();
    }

    public function getTabLabel() {
        return __("Main Information");
    }

    public function getTabTitle() {
        return __("Main Information");
    }

    public function canShowTab() {
        return true;
    }

    public function isHidden() {
        return false;
    }
}