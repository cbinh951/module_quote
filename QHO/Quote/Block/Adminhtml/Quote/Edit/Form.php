<?php 

namespace QHO\Quote\Block\Adminhtml\Quote\Edit;

use Magento\Backend\Block\Widget\Form\Generic;

class Form extends Generic {
    protected function _construct() {
        $this->setId("quote_form");
        $this->setTitle(__("Staff Information"));
        parent::_construct();
    }

    protected function _prepareForm() {
        $form = $this->_formFactory->create(
            [
                "data" =>   [
                    "id"    =>  "edit_form",
                    "action"    =>  $this->getData("action"), //save
                    "method"    =>  "post",
                ]
            ]
        );

        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }
}