<?php 

namespace QHO\Quote\Block\Adminhtml\Quote\Edit;

use Magento\Backend\Block\Widget\Tabs as WidgetTabs;

class Tabs extends WidgetTabs {
    public function  _construct() {
        parent::_construct();
        $this->setId("quote_show_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(__("Quote Manager"));
    }

    protected function _beforeToHtml() {
        $this->addTab(
            "quote_main",
            [
                "label" =>  __("Customer View"),
                "title" =>  __("Customer View"),
                "content"   =>  $this->getLayout()->createBlock(
                    "QHO\Quote\Block\Adminhtml\Quote\Edit\Tab\Customer")->toHtml(),
                "active"    =>  true
            ]
        );

        $this->addTab(
            "quote_address",
            [
                "label" =>  __("Product Reviews"),
                "title" =>  __("Product Reviews"),
                "content"   =>  $this->getLayout()->createBlock(
                    "QHO\Quote\Block\Adminhtml\Quote\Edit\Tab\Product")->toHtml(),
                "active"    =>  false
            ]
        );

        return parent::_beforeToHtml();
    }
}