<?php

namespace QHO\Quote\Block\Adminhtml\Quote;

use Magento\Backend\Block\Widget\Form\Container;

class Show extends Container {
    protected function _construct() {
        $this->_blockGroup = "QHO_Quote";
        $this->_controller = "adminhtml_quote";
        $this->_objectId = "entity_id";
        parent::_construct();
        $this->buttonList->update("save", "label", __("Contacted"));
    }

    public function getSaveUrl()
    {
        return $this->getUrl('quote/index/contacted');
    }
}