<?php
namespace QHO\Quote\Block\Adminhtml\Quote;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{
    protected $_collectionFactory;

    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \QHO\Quote\Model\ResourceModel\Quote\CollectionFactory $collectionFactory,
        array $data = []
    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setId('quoteGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        // $this->setUseAjax(true);
        $this->setSaveParametersInSession(true);
    }

    /**
     * Prepare collection
     *
     * @return \Magento\Backend\Block\Widget\Grid
     */
    protected function _prepareCollection()
    {
        $collection = $this->_collectionFactory->create();
        $collection->getListQuote();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * Prepare columns
     *
     * @return \Magento\Backend\Block\Widget\Grid\Extended
     */
    protected function _prepareColumns()
    {
        $this->addColumn('entity_id', [
            'header' => __('ID'),
            'index' => 'entity_id',
        ]);

        $this->addColumn('email', ['header' => __('Email'), 'index' => 'email']);
        $this->addColumn('telephone', ['header' => __('Mobile Number'), 'index' => 'telephone']);
        
        $this->addColumn(
            'created_at',
            [
                'header' => __('Created'),
                'index' => 'created_at',
                'type' => 'datetime',
                'header_css_class' => 'col-date',
                'column_css_class' => 'col-date'
            ]
        );

        $this->addColumn(
            'action',
            [
                'header' => __('Action'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('View'),
                        'url' => [
                            'base' => '*/*/show',
                            'params' => ['id' => $this->getRequest()->getParam('id')]
                        ],
                        'field' => 'id'
                    ],
                    [
                        'caption' => __('Contacted'),
                        'url' => [
                            'base' => '*/*/contacted',
                            'params' => ['id' => $this->getRequest()->getParam('id')]
                        ],
                        'field' => 'id'
                    ]
                ],
                'sortable' => false,
                'filter' => false,
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action'
            ]
        );

        return parent::_prepareColumns();
    }

    /**
    * Row click url
    *
    * @param \Magento\Framework\Object $row
    * @return string
    */
    public function getRowUrl($row)
    {
            return $this->getUrl('*/*/show', ['id' => $row->getId()]);
    }
}