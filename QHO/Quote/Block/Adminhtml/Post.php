<?php
namespace QHO\Quote\Block\Adminhtml;

class Post extends \Magento\Backend\Block\Widget\Grid\Container
{
	protected function _construct()
	{
		$this->_controller = 'adminhtml_quote';
		$this->_blockGroup = 'QHO_Quote';
		$this->_headerText = __('Posts');
		parent::_construct();

        $this->buttonList->remove('add');
	}
}