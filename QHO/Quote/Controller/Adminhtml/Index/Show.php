<?php

namespace QHO\Quote\Controller\Adminhtml\Index;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use QHO\Quote\Model\QuoteFactory;
use Magento\Framework\Registry;
use Magento\Framework\App\ResourceConnection;

class Show extends \Magento\Backend\App\Action {
    protected $_resultPageFactory;
    protected $_quoteFactory;
    protected $_coreRegistry;

    protected $_resource;

    public function __construct(
            Context $context, 
            PageFactory $pageFactory,
            QuoteFactory $quoteFactory,
            Registry $registry,
            ResourceConnection $resource) {
        parent::__construct($context);
        $this->_resultPageFactory = $pageFactory;
        $this->_quoteFactory = $quoteFactory;
        $this->_coreRegistry = $registry;

        $this->_resource = $resource;
    }

    public function execute() {
        $quoteId = $this->getRequest()->getParam("id");

        $model = $this->_quoteFactory->create();

        $connection = $this->_objectManager->create('\Magento\Framework\App\ResourceConnection');
        // $connection = $this->_objectManager->create($this->_resource);
        $conn = $connection->getConnection();
        $select = $conn->select()
            ->from(
                ['main_table' => 'quote']
            )
            ->join(
                ['secondTable' => 'quote_address'],
                'main_table.entity_id = secondTable.quote_id'
            )->join(
                ['thirdTable' => 'quote_item'],
                'main_table.entity_id = thirdTable.quote_id'
            )
            ->where("(secondTable.email IS NOT NULL OR secondTable.telephone IS NOT NULL) 
            AND secondTable.address_type = 'shipping' 
            AND  items_count > 0 AND main_table.entity_id = $quoteId");

        $quote_item = $conn->fetchAll($select);

        if ($quoteId) {
             $model->load($quoteId);
             $model->getQuoteById($quoteId);
             if (!$model->getId()) {
                 $this->messageManager->addError(__("This quote no longer exists"));
                 return $this->_redirect("*/*/");
             }
            $title = "Show A Quote " . $model->getName();
        } 

        $this->_coreRegistry->register("quote", $quote_item);
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu("QHO_Quote::quote");
        $resultPage->getConfig()->getTitle()->prepend(__($title));
       
        return $resultPage;
    }
}