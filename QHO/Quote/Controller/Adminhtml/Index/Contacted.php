<?php

namespace QHO\Quote\Controller\Adminhtml\Index;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use QHO\Quote\Model\QuoteFactory;

use \Magento\Framework\App\ResourceConnection;

class Contacted extends \Magento\Backend\App\Action {
    protected $_resultPageFactory;
    protected $_quoteFactory;

    protected $_resource;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        QuoteFactory $quoteFactory) {
        parent::__construct($context);
        $this->_quoteFactory = $quoteFactory;
    }

    public function execute() {
        $quoteId = $this->getRequest()->getParam("id"); 
        $model = $this->_quoteFactory->create();

        $data = $model->load($quoteId);
        $itemQuote = $data->getData();
        $isContacted = !$itemQuote['is_contact'];
        $isContacted =  $data->setIsContact($isContacted)->save();

        $this->_redirect('*/*/');
    }
}