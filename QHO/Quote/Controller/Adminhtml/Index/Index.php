<?php

namespace QHO\Quote\Controller\Adminhtml\Index;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Backend\App\Action {
    public function __construct(Context $context, PageFactory $pageFactory) {
        parent::__construct($context);
        $this->_resultPageFactory = $pageFactory;
    }

    public function execute() {
        $this->_view->loadLayout(false);
        $this->_view->renderLayout();
    }
}